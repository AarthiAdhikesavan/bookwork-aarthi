import math

def factorial(n):
    if n==0:
        return 1
    else:
        recursion = factorial(n-1)
        result= n*recursion
        return result
def estimate_pi():
    factor = 2*(math.sqrt(2)) / 9801
    k=0
    total=0
    while True:
        num=factorial(4*k)*(1103+26390*k)
        denominator=factorial(k)**4 * 396**(4*k)
      
        term=factor*num/ denominator
        total+= term
        if abs(term) < 1e-15:
             break
        k+=1
    return 1 / total
        
   
print estimate_pi()

    

