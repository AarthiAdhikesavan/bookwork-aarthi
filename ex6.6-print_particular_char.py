def first(word):
    return word[0]

def last(word):
    return word[-1]

def middle(word):
    return word[1:-1]

print first('man')
print last('man')
print middle('man')
print middle('an')
print middle('a')
print middle('')
